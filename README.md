# sunxi-livesuit

This repo is my personal modifications to sunxi-livesuit which builds & works correctly under Linux Mint 19.x+

Should work on any Ubuntu 18.04+ distro

Tested with Android 7.1 and Android 8.1 images on Bananapi-m64 board.

For building & running, allways use sudo !!!

# Building

```
git clone git@gitlab.com:stulluk/sunxi-livesuit.git
cd sunxi-livesuit
sudo apt install dkms
cd awusb/
sudo make
sudo cp awusb.ko /lib/modules/`uname -r`/kernel/
sudo depmod -a
sudo modprobe awusb


```

And then, create following file:

```
sudo vi /etc/udev/rules.d/50-awusb.rules
```

Insert following:

```
KERNEL=="aw_efex[0-9]*", MODE="0666"
```

And finally:

```
sudo udevadm control --reload-rules
```

# Running

```
sudo ./LiveSuit.sh
```

# Debugging

Use:

```
sudo dmesg -w
```

Try to see if your device is detected correctly.

Use short USB cables, make sure you are powered your board correctly..
